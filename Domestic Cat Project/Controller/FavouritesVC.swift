//
//  FavouritesVC.swift
//  Domestic Cat Project
//
//  Created by Raunak Singh on 19/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import CoreData

class FavouritesVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var likedRestaurants = [LikedRestaurants]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        retriveData()
        tableView.reloadData()
    }
    @IBAction func unlikeButtonPressed(_ sender: UIButton) {
        let row = (self.tableView.indexPathForView(sender)?.row)
        let unliked = likedRestaurants[row!]

        context.delete(unliked)
        likedRestaurants.remove(at: row!)
        tableView.reloadData()

        ad.saveContext()
        }
    
}

extension FavouritesVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return likedRestaurants.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "favouriteCell") as? FavouriteCell {
            
            cell.updateFavouriteCellUI(restuarant: likedRestaurants[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
}

extension FavouritesVC {
    
    func retriveData() {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LikedRestaurants")
        do {
            if let results = try context.fetch(fetchRequest) as? [LikedRestaurants]  {
                likedRestaurants = results
                print("retrived.count = \(likedRestaurants.count)")
                print(likedRestaurants.count)
                self.tableView.reloadData()
            }
        } catch {
            fatalError("Error")
        }
    }
    
}
