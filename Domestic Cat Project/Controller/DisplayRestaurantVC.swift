//
//  ViewController.swift
//  Domestic Cat Project
//
//  Created by Raunak Singh on 12/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit
import CoreData

class DisplayRestaurantVC: UIViewController {
    
    let networkingObj = Networking()
    var returnedRestaurants = [Restaurant]()
    var persistedSavedRestaurants = [SavedRestaurants]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if Connectivity.isConnectedToInternet(){
            fetchLatestData()
        } else {
            Alert.showBasic(title: "No Internet Connection", message: "The App will show persistedData, as there's no internet connection. Connect to internet to get the latest data.", vc: self)
            fetchPersistedData()
        }
    }
    

}

extension DisplayRestaurantVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if Connectivity.isConnectedToInternet() {
            print("displaying New data")
            return returnedRestaurants.count
        } else {
            print("displaying persisted data...........")
            return persistedSavedRestaurants.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Connectivity.isConnectedToInternet() {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantCell") as? RestaurantCell {
                cell.updateCellUI(restaurantObj: returnedRestaurants[indexPath.row])
                return cell
            } else {
                return UITableViewCell()
        }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantCell") as? RestaurantCell {
                cell.persistCellUI(savedRestaurantObj: persistedSavedRestaurants[indexPath.row])
                return cell
            } else {
                return UITableViewCell()
        }
    }
    }
}

extension DisplayRestaurantVC {
    
    func fetchLatestData() {
        networkingObj.getLatestRestautantData { (status, restaurants) in
            if status {
                self.returnedRestaurants = restaurants
                print(self.returnedRestaurants.count)
                self.tableView.reloadData()
                self.persistNewData(restaurantsArray: self.returnedRestaurants)
            } else {
                Alert.showBasic(title: "Can't access Zomato API.", message: "Currently Zomato API can't be accessed as of now. Displaying retrived Data.", vc: self)
                // show retrived data.
            }
        }
        
    }
    
    func fetchPersistedData() {
        if persistedSavedRestaurants.count > 0 {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedRestaurants")
        do {
            if let results = try context.fetch(fetchRequest) as? [SavedRestaurants]  {
                persistedSavedRestaurants = results
                print(persistedSavedRestaurants.count)
                print(persistedSavedRestaurants[0].image as Any)
                self.tableView.reloadData()
            }
        } catch {
            fatalError("Error")
        }
        
        }
        
    }
    
    func deleteAllPersistedData() {
        if persistedSavedRestaurants.count > 0 {
        let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "SavedRestaurants")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
        
        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            fatalError("Can't perform delete.")
        }
        }
    }
    
    func persistNewData(restaurantsArray: [Restaurant]) {
        deleteAllPersistedData()
                print(restaurantsArray.count)
        for restaurantObj in restaurantsArray {
            let restaurant = SavedRestaurants(context: context)
            restaurant.name = restaurantObj.restaurantName
            restaurant.address = restaurantObj.restaurantAddress
            restaurant.imageUrl = restaurantObj.restuarantImage
            
            networkingObj.getImage(url: restaurantObj.restuarantImage) { (downloadedImage) in
                let imageData: Data = UIImageJPEGRepresentation(downloadedImage, 1) as! Data
                restaurant.image = imageData
                print(imageData)
                print(restaurantObj.restaurantName)
            }
            
        }
        ad.saveContext()
    }
}

extension DisplayRestaurantVC {
    @IBAction func LikeBtnPressed(_ sender: UIButton) {
        
        if !Connectivity.isConnectedToInternet() {
            Alert.showBasic(title: "Can't Favourite", message: "Can't add favourites when not connected to internet", vc: self)
            return
        }
        let liked = returnedRestaurants[(self.tableView.indexPathForView(sender)?.row)!]
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "LikedRestaurants")
        do {
            if let results = try context.fetch(fetchRequest) as? [LikedRestaurants]  {
                for result in results {
                    if result.name == liked.restaurantName {
                        Alert.showBasic(title: "Restaurant already liked.", message: "This retaurant is already added to your favourite list.", vc: self)
                        return
                    }
                }
                        let likedRestaurant = LikedRestaurants(context: context)
                        likedRestaurant.name = liked.restaurantName
                        likedRestaurant.address = liked.restaurantAddress
                        networkingObj.getImage(url: liked.restuarantImage) { (downloadedImage) in
                            let imageData: Data = UIImageJPEGRepresentation(downloadedImage, 1) as! Data
                            likedRestaurant.image = imageData
                    }
                ad.saveContext()
            }
        } catch {
            fatalError("Error")
        }
        
    }
}



