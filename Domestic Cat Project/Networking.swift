//
//  Networking.swift
//  Domestic Cat Project
//
//  Created by Raunak Singh on 14/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage

class Networking {

     let imageCache = NSCache<NSString, UIImage>()
    
    func getLatestRestautantData(downloadComplete: @escaping (_ status : Bool, _ returnedestaurants : [Restaurant]) ->()) {
        let headers = ["Accept" : "application/json","user-key" : "5242198974b9ec4765ce08c5b279da58"]
        var restaurants = [Restaurant]()
        Alamofire.request(baseZomatoApi, headers: headers)
            .responseJSON { response in
                let returnedData = response.result.value as! Dictionary<String,Any>
                if let topRestaurants = returnedData["best_rated_restaurant"] as? [Dictionary<String,Any>] {
                    for restaurant in topRestaurants{
                        if let restaurantData = restaurant["restaurant"] as? Dictionary<String,Any> {
                            let name = restaurantData["name"] as? String
                            let imageUrl = restaurantData["featured_image"] as? String
                            if let locationData = restaurantData["location"] as? Dictionary<String,Any> {
                                let address = locationData["address"] as? String
                                let restaurantObj = Restaurant(restuarantImage: imageUrl!, restaurantName: name!, restaurantAddress: address!)
                                restaurants.append(restaurantObj)
                            }
                        }
                    }
                }
                downloadComplete(true,restaurants)
        }
        
    }
    
    func getImage(url: String, completion : @escaping (_ downloadedImage: UIImage) ->()) {
        if let imageFromCache = imageCache.object(forKey: url as NSString) {
            completion(imageFromCache)
        } else {
            downloadImage(url: url, completion: completion)
        }
    }
    
    func downloadImage(url: String, completion : @escaping (_ downloadedImage: UIImage) ->()) {
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                let imageToCache = image
                self.imageCache.setObject(imageToCache, forKey: url as NSString)
                completion(imageToCache)
            } else {
                return
            }
           
        }
    }

}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}

