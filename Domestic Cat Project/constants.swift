//
//  constants.swift
//  Used to store constants such as API, Lat and Long.
//  Domestic Cat Project
//
//  Created by Raunak Singh on 13/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

let baseZomatoApi = "https://developers.zomato.com/api/v2.1/location_details?entity_id=98284&entity_type=subzone"
let zomatoApiKey = "5242198974b9ec4765ce08c5b279da58"


// These are the Lat and Long constants for Abbotsford, if using current location CoreLocation API can be used.
let abbotsfordLat = "-37.803283"
let abbotsfordLong = "144.997098"

