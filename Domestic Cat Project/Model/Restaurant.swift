//
//  Restuarant.swift
//  Domestic Cat Project
//
//  Created by Raunak Singh on 12/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation

struct Restaurant {
    var restuarantImage : String
    var restaurantName : String
    var restaurantAddress : String
}
