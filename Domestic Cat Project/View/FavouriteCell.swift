//
//  FavouriteCell.swift
//  Domestic Cat Project
//
//  Created by Raunak Singh on 19/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit

class FavouriteCell: UITableViewCell {
    
    @IBOutlet weak var favouriteRestaurantImage: UIImageView!
    @IBOutlet weak var favouriteRestaurantName: UILabel!
    @IBOutlet weak var favouriteRestaurantAddress: UILabel!
    
    func updateFavouriteCellUI(restuarant: LikedRestaurants) {
        favouriteRestaurantName.text = restuarant.name
        favouriteRestaurantAddress.text = restuarant.name
        if restuarant.image == nil {
            favouriteRestaurantImage.image = UIImage(named: "food")
        } else {
            favouriteRestaurantImage.image = UIImage(data: restuarant.image!)
        }
        
        if favouriteRestaurantImage.layer.sublayers == nil {
            favouriteRestaurantImage.setImageViewGradient()
        }
    }
}
