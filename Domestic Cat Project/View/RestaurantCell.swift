//
//  RestaurantCell.swift
//  Domestic Cat Project
//
//  Created by Raunak Singh on 12/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import UIKit


class RestaurantCell: UITableViewCell {
    
    let networkingObj = Networking()

    @IBOutlet weak var restaurantImageView: UIImageView!
    @IBOutlet weak var restaurantNameLbl: UILabel!
    @IBOutlet weak var restaurantAddressLbl: UILabel!
    
    func updateCellUI(restaurantObj : Restaurant) {
        
        restaurantNameLbl.text = restaurantObj.restaurantName
        restaurantAddressLbl.text = restaurantObj.restaurantAddress
        
        let imageURL = restaurantObj.restuarantImage
        networkingObj.getImage(url: imageURL) { (downloadedImage) in
            self.restaurantImageView.image = downloadedImage
        }
        
        if restaurantImageView.layer.sublayers == nil {
        restaurantImageView.setImageViewGradient()
        }
    }
    
    func persistCellUI(savedRestaurantObj: SavedRestaurants) {
        restaurantNameLbl.text = savedRestaurantObj.name
        restaurantAddressLbl.text = savedRestaurantObj.address
        if savedRestaurantObj.image == nil {
            restaurantImageView.image = UIImage(named: "food")
        } else {
            restaurantImageView.image = UIImage(data: savedRestaurantObj.image!)
        }
        if restaurantImageView.layer.sublayers == nil {
            restaurantImageView.setImageViewGradient()
        }

    }
    
    
    

}
