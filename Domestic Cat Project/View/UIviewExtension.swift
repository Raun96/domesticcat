//
//  UIviewExtension.swift
//  Domestic Cat Project
//
//  Created by Raunak Singh on 12/9/18.
//  Copyright © 2018 Raunak Singh. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func indexPathForView(_ view: UIView) -> IndexPath? {
        let center = view.center
        let viewCenter = self.convert(center, from: view.superview)
        let indexPath = self.indexPathForRow(at: viewCenter)
        return indexPath
    }
}

extension UIView {
    
    func setImageViewGradient() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors =  [UIColor.clear.cgColor , UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0.7).cgColor]
        gradientLayer.locations = [0,1]
        self.layer.addSublayer(gradientLayer)
    }
    
    
}
